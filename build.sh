#!/bin/bash

docker build --tag=$(id -u -n)/tensorflow:latest-opencv-jupyter --build-arg USER_ID=$(id -u) --build-arg GROUP_ID=$(id -g) ./container
