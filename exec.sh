#!/bin/bash

IMAGE_NAME=$(id -u -n)/tensorflow:latest-opencv-jupyter
CONTAINER_NAME=$(echo -n $IMAGE_NAME | md5sum | awk '{print $1;}')

docker exec -it $(docker ps -aqf "name=$CONTAINER_NAME") bash
