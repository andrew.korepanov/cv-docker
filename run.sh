#!/bin/bash

IMAGE_NAME=$(id -u -n)/tensorflow:latest-opencv-jupyter
CONTAINER_NAME=$(echo -n $IMAGE_NAME | md5sum | awk '{print $1;}')

WORK_PATH=~/work
DATA_PATH=~/data

docker run --rm -it -p 38888:8888 -p 36006:6006 \
    -v ~/.bash_history:/home/me/.bash_history \
    -v $WORK_PATH:/home/me/work \
    -v $DATA_PATH:/home/me/data \
    --name $CONTAINER_NAME \
    $IMAGE_NAME
